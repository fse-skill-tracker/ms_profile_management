package com.cts.skilltracker.ms_profile_management.repository;

import com.cts.skilltracker.ms_profile_management.model.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserProfileRepository extends JpaRepository<UserProfile, Long> {
    public List<UserProfile> findByAssociateId(String associateId);
    public List<UserProfile> findByNameStartsWith(String name);
    public List<UserProfile> findByAssociateIdIn(List<String> associateId);
}
