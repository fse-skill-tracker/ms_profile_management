package com.cts.skilltracker.ms_profile_management.model;

import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UserSkill {

    @Valid
    private UserProfile userProfile;
    @Valid
    private TechSkills techSkills;
    @Valid
    private SoftSkills softSkills;
}
