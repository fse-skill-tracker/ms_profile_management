package com.cts.skilltracker.ms_profile_management;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
public class MsProfileManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsProfileManagementApplication.class, args);
	}

}
