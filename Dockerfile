FROM maven:3.3-jdk-8 as build
ARG CI_PROJECT_DIR=CODE_PATH
WORKDIR={CI_PROJECT_DIR}
RUN mvn clean package

FROM openjdk:8u111-jdk-alpine
ARG JAR_FILE=CODE_PATH/target/*.jar
EXPOSE 9092
WORKDIR /opt/target
COPY -- from build {JAR_FILE} ms_profile_management-0.0.1-SNAPSHOT.jar
ENTRYPONT ["java", "-jar", "/ms_profile_management-0.0.1-SNAPSHOT.jar"]