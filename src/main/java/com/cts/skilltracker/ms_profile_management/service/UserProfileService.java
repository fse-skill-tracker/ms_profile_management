package com.cts.skilltracker.ms_profile_management.service;

import com.cts.skilltracker.ms_profile_management.model.SkillTrackerEvent;
import com.cts.skilltracker.ms_profile_management.model.UserProfile;
import com.cts.skilltracker.ms_profile_management.model.UserSkill;
import com.cts.skilltracker.ms_profile_management.repository.UserProfileRepository;
import org.apache.kafka.clients.admin.NewTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserProfileService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserProfileService.class);
    @Autowired
    private UserProfileRepository userRepository;

    private NewTopic topic;

    private KafkaTemplate<String, SkillTrackerEvent> kafkaTemplate;

    public UserProfileService(NewTopic topic, KafkaTemplate<String, SkillTrackerEvent> kafkaTemplate) {
        this.topic = topic;
        this.kafkaTemplate = kafkaTemplate;
    }

    public String addUser(UserSkill userSkill) {
        UserProfile userProf = userSkill.getUserProfile();
        userRepository.save(userSkill.getUserProfile());

        SkillTrackerEvent stevent = new SkillTrackerEvent();
        stevent.setAssociateId(userProf.getAssociateId());
        stevent.setSoftSkills(userSkill.getSoftSkills());
        stevent.setTechSkills(userSkill.getTechSkills());

        sendMessage(stevent);
        return "Success";
    }

    private void sendMessage(SkillTrackerEvent stevent){
        Message<SkillTrackerEvent> message = MessageBuilder
                .withPayload(stevent)
                .setHeader(KafkaHeaders.TOPIC,topic.name())
                .build();
        LOGGER.info(String.format("Skill Event %s", stevent));
        kafkaTemplate.send(message);
    }

    public List<UserProfile> searchUserByName(String name){
        List<UserProfile> usersList =  userRepository.findByNameStartsWith(name);
        return usersList;
    }

    public List<UserProfile> searchUserByAssociateId(String associateId){
        List<UserProfile> usersList =  userRepository.findByAssociateId(associateId);
        return usersList;
    }

    public List<UserProfile> searchUsersByAssociateList(List<String> associateList) {
        List<UserProfile> userProfiles = userRepository.findByAssociateIdIn(associateList);
        return userProfiles;
    }
}
