package com.cts.skilltracker.ms_profile_management.controller;


import com.cts.skilltracker.ms_profile_management.exceptionHandler.CustomExceptionDetail;
import com.cts.skilltracker.ms_profile_management.exceptionHandler.CustomExceptionHandler;
import com.cts.skilltracker.ms_profile_management.model.SoftSkills;
import com.cts.skilltracker.ms_profile_management.model.TechSkills;
import com.cts.skilltracker.ms_profile_management.model.UserProfile;
import com.cts.skilltracker.ms_profile_management.model.UserSkill;
import com.cts.skilltracker.ms_profile_management.service.UserProfileService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.HttpClientErrorException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
class UserProfileControllerTest {

    @InjectMocks
    private UserProfileController userProfileController;

    @Mock
    private UserProfileService userProfileService;


    private MockMvc mockMvc;

    @BeforeEach
    public void beforeEach(){
        MockitoAnnotations.openMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(userProfileController)
        .setControllerAdvice(new CustomExceptionHandler()).build();
    }

    @Test
    void addUser() {
        UserSkill userSkill = UserSkill.builder()
                .userProfile(UserProfile.builder()
                        .associateId("CTS001").email("abc@gmail.com")
                        .mobile("1231230000")
                        .name("TestUser")
                        .build())
                .techSkills(TechSkills.builder()
                        .angular(10)
                        .spring(14)
                        .htmlJS(7)
                        .build())
                .softSkills(SoftSkills.builder().spoken(15).build())
                .build();
        ResponseEntity response =  userProfileController.addUser(userSkill);
        Mockito.verify(userProfileService).addUser(userSkill);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    void addInvalidUser() throws Exception {
        UserSkill userSkill = UserSkill.builder()
                .userProfile(UserProfile.builder()
                        .associateId("ABC001").email("abc@gmail.com")
                        .mobile("1231230000")
                        .name("TestUser")
                        .build())
                .techSkills(TechSkills.builder()
                        .angular(10)
                        .spring(24)
                        .htmlJS(7)
                        .build())
                .softSkills(SoftSkills.builder().spoken(15).build())
                .build();

        ObjectWriter ow =  new ObjectMapper().writer().withDefaultPrettyPrinter();
        String body = ow.writeValueAsString(userSkill);

        mockMvc.perform(post("/skill-tracker/users/add-profile")
                .contentType(MediaType.APPLICATION_JSON)
                .content(body))
                .andExpect(status().isBadRequest());
        Mockito.verify(userProfileService, times(0)).addUser(userSkill);
       // Assertions.assertThrows(HttpClientErrorException.class, ()->userProfileController.addUser(userSkill));
    }

    @Test
    void searchUserByName() {
    }

    @Test
    void searchUserByAssociates() {
    }

    @Test
    void searchUserByAssociateId() {
    }
}