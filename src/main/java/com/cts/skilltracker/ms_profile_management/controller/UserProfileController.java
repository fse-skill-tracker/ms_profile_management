package com.cts.skilltracker.ms_profile_management.controller;

import com.cts.skilltracker.ms_profile_management.model.UserProfile;
import com.cts.skilltracker.ms_profile_management.model.UserSkill;
import com.cts.skilltracker.ms_profile_management.service.UserProfileService;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Validated
@RestController
@RequestMapping("/skill-tracker/users")
public class UserProfileController {

    @Autowired
    private UserProfileService profileService;

    @PostMapping("/add-profile")
    public ResponseEntity<?> addUser(@Valid @RequestBody UserSkill userSkill){
     String response =    profileService.addUser(userSkill);
     return new ResponseEntity<>(response,HttpStatus.OK);
    }

    @GetMapping("/health-check")
    public String healthCheck(){
        return "true";
    }

    @GetMapping("/search-user/name/{name}")
    public ResponseEntity<List<UserProfile>> searchUserByName(@PathVariable String name){
        HttpStatus status = HttpStatus.OK;
        List<UserProfile> users = profileService.searchUserByName(name);
        if(users == null || users.size()==0){
            status =  HttpStatus.NOT_FOUND;
        }
        return  ResponseEntity.status(status).body(users);

    }

    @GetMapping("/search-user/byAssociates")
    public ResponseEntity<List<UserProfile>> searchUserByAssociates(@RequestParam List<String> associateList){
        HttpStatus status = HttpStatus.OK;
        List<UserProfile> users = profileService.searchUsersByAssociateList(associateList);
        if(users == null || users.size()==0){
            status =  HttpStatus.NOT_FOUND;
        }
        return  ResponseEntity.status(status).body(users);
    }

    @GetMapping("/search-user/associateId/{associateId}")
    public ResponseEntity<List<UserProfile>> searchUserByAssociateId(@PathVariable String associateId){
        HttpStatus status = HttpStatus.OK;
        List<UserProfile> users = profileService.searchUserByAssociateId(associateId);
        if(users == null || users.size()==0){
            status =  HttpStatus.NOT_FOUND;
        }
        return  ResponseEntity.status(status).body(users);
    }
}
